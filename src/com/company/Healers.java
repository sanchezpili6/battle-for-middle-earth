package com.company;

public class Healers extends Items {
    public Healers(){
    }

    public void elfgetHealer(Elfs elf){
        if(!this.isDestroyed()){
            elf.setLife(elf.getLife()+elf.getLife()/2);
            System.out.println("El elfo "+elf.getName()+ " encontró una cura.");
            System.out.println(elf.getName()+" ahora tiene "+elf.getLife()+" puntos de vida.");
            this.setDestroyed(true);
        }
    }

    public void orcgetHealer(Orcs orc){
        if(!this.isDestroyed()){
            orc.setLife(orc.getLife()+orc.getLife()/2);
            System.out.println("El orco " +orc.getName() +" encontró una cura");
            System.out.println(orc.getName()+ " ahora tiene "+ orc.getLife()+" puntos de vida.");
            this.setDestroyed(true);
        }
    }

    public void trollgetHealer(Trolls troll){
        if(!this.isDestroyed()){
            System.out.println("Un Troll encontró una cura y la destruyó");
            this.setDestroyed(true);
        }
    }

}
