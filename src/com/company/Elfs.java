package com.company;

public class Elfs extends Creatures{
    String[] elfNames= {"Elden", "Eldan", "Eldon", "Ealhdun", "Fafnir", "Aelfdane", "Arya", "Anduin", "Dras", "Beren"};
    Elfs(){
        this.setStrength(Orcs.random(20, 50));
        this.setMagic(Orcs.random(60,120));
        this.setLife(Orcs.random(1500, 2000));
        this.setName(elfNames[Orcs.random(0,elfNames.length-1)]);

    }
    public int getHorde() {
        return horde;
    }

    public void setHorde(int horde) {
        this.horde = horde;
    }
}
