package com.company;

import java.util.Random;

public class Orcs extends Creatures {
    String[] orcNames={"Grom","Thrum", "Drog", "Gorrum", "Karg", "Hargu", "Igrim", "Agra", "Dragga", "Grima"};

    public Orcs(){
        this.setStrength(random(50,140));
        this.setMagic(random(1, 10));
        this.setLife(random(1000, 2500));
        this.setName(orcNames[random(0,orcNames.length -1)]);
    }


    public static int random(int a, int b){
        Random randomGenerator = new Random();
        return randomGenerator.nextInt(b)+a;
    }
    public int getHorde() {
        return horde;
    }

    public void setHorde(int horde) {
        this.horde = horde;
    }
}
