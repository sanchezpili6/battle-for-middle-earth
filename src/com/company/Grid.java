package com.company;

public class Grid {

    private Orcs[] orcs = new Orcs[4];
    private Elfs[] elfs = new Elfs[4];
    private Trolls[] trolls = new Trolls[1];
    private Hordes[] elfHordes = new Hordes[5];
    private Hordes[] orcHordes = new Hordes[5];
    private Amulets[] amulets = new Amulets[2];
    private Weapons[] weapons = new Weapons[2];
    private Healers[] healers = new Healers[2];

    private Grid(){
        for(int i= 0; i<orcs.length; i++) {
            orcs[i]= new Orcs();
           }
        for(int i=0; i<elfs.length; i++){
            elfs[i]= new Elfs();
        }
        for(int i=0; i<trolls.length; i++){
            trolls[i]= new Trolls();
        }
        this.weapons[0] = new Weapons();
        this.weapons[1] = new Weapons();
        this.amulets[0] = new Amulets();
        this.amulets[1] = new Amulets();
        this.healers[0] = new Healers();
        this.healers[1] = new Healers();

    }


    private void oneOnOneFight(Elfs a, Orcs b){
        if(a.isAlive() && b.isAlive()){
            a.setLife(a.getLife()-b.getDamageDone());
            b.setLife(b.getLife()-a.getDamageDone());
            if(a.getLife()>b.getLife()){
                System.out.println("El elfo "+a.getName()+" ganó la batalla.");
                b.setLife(0);
                System.out.println("El orco "+b.getName()+" murió.");
            }
            else if(b.getLife()>a.getLife()){
                System.out.println("El Orco "+b.getName()+" ganó la batalla");
                a.setLife(0);
                System.out.println("El elfo "+a.getName()+" murió.");
            }
        }
        printPos();
    }
    private boolean areElfsAlive(){
        int alive = 0;
        for (Elfs elf : elfs) {
            if (elf.isAlive()) {
                alive += 1;
            }
            if (alive > 0) {
                return true;
            }

        }
        return false;
    }
    private boolean areOrcsAlive(){
        int alive = 0;
        for (Orcs orc : orcs) {
            if (orc.isAlive()) {
                alive += 1;
            }
            if (alive > 0) {
                return true;
            }
        }
        return false;
    }


    private void printPos(){
        System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Orcos: \n");
        if(!areOrcsAlive()){
            System.out.println("No quedan Orcos vivos.");
        }
        else {
            for (Orcs orc : orcs) {
                if(orc.isAlive()){
                    System.out.println("El Orco " + orc.getName() + " está en la posición " + orc.getCoordx() + " en X, y en la posición " + orc.getCoordy() + " en Y.");
                }
                else{
                    System.out.println("El Orco "+orc.getName()+" está muerto.");
                }

            }
        }
        System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Elfos: \n");
        if(!areElfsAlive()){
            System.out.println("No quedan Elfos vivos.");
        }
        else {
            for (Elfs elf : elfs) {
                if(elf.isAlive()){
                    System.out.println("El Elfo " + elf.getName() + " está en la posición " + elf.getCoordx() + " en X, y en la posición " + elf.getCoordy() + " en Y.");
                }
                else{
                    System.out.println("El Elfo "+elf.getName()+" está muerto.");
                }
            }
        }
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------");

    }
    private void getElfHordes(Hordes a, Elfs b) {
        a.setMembers(b);
        int cx = b.getCoordx();
        int cy = b.getCoordy();

        a.centroidX(cx);
        a.centroidY(cy);
        b.setCoordx(a.getCoordX());
        b.setCoordy(a.getCoordY());
    }

    private void getOrcHordes(Hordes a, Orcs b){
        a.setMembers(b);
        int cx = b.getCoordx();
        int cy = b.getCoordy();

        a.centroidX(cx);
        a.centroidY(cy);
        b.setCoordx(a.getCoordX());
        b.setCoordy(a.getCoordY());
    }

    private void moveHordes(){
        for (Hordes elfHorde : elfHordes) {
            int a = elfHorde.getNumMembers();
            for (int j = 0; j < a; j++) {
                elfHorde.getMembers(j).moveX();
                elfHorde.getMembers(j).moveY();
            }
        }
        for (Hordes orcHorde : orcHordes) {
            int a = orcHorde.getNumMembers();
            for (int j = 0; j < a; j++) {
                orcHorde.getMembers(j).moveX();
                orcHorde.getMembers(j).moveY();
            }
        }
    }

    private void fight(){
        if(areElfsAlive()||areOrcsAlive()){
            printPos();
            moveHordes();
        //encuentran amuletos
            for (Elfs elf : elfs) {
                for (Amulets amulet : amulets) {
                    if (betweenElfAmulet(elf, amulet) <= 5 && !amulet.isDestroyed()) {
                        amulet.elfgetAmulet(elf);
                    }
                }
            }
            for (Orcs orc : orcs) {
                for (Amulets amulet : amulets) {
                    if (betweenOrcAmulet(orc, amulet) < 5 && !amulet.isDestroyed()) {
                        amulet.orcgetAmulet(orc);
                    }
                }
            }
            for (Trolls troll : trolls) {
                for (Amulets amulet : amulets) {
                    if (betweenTrollAmulet(troll, amulet) < 5 && !amulet.isDestroyed()) {
                        amulet.trollgetAmulet(troll);
                    }
                }
            }
        //encuentran armas
            for (Elfs elf : elfs) {
                for (Weapons weapon : weapons) {
                    if (betweenElfWeapon(elf, weapon) <= 5 && !weapon.isDestroyed()) {
                        weapon.elfgetWeapon(elf);
                    }
                }
            }
            for (Orcs orc : orcs) {
                for (Weapons weapon : weapons) {
                    if (betweenOrcWeapon(orc, weapon) < 5 && !weapon.isDestroyed()) {
                        weapon.orcgetWeapon(orc);
                    }
                }
            }
            for (Trolls troll : trolls) {
                for (Weapons weapon : weapons) {
                    if (betweenTrollWeapon(troll, weapon) < 5 && !weapon.isDestroyed()) {
                        weapon.trollgetWeapon(troll);
                    }
                }
            }
        //encuentran healers
            for (Elfs elf : elfs) {
                for (Healers healer : healers) {
                    if (betweenElfHealer(elf, healer) <= 5 && !healer.isDestroyed()) {
                        healer.elfgetHealer(elf);
                    }
                }
            }
            for (Orcs orc : orcs) {
                for (Healers healer : healers) {
                    if (betweenOrcHealer(orc, healer) < 5 && !healer.isDestroyed()) {
                        healer.orcgetHealer(orc);
                    }
                }
            }
            for (Trolls troll : trolls) {
                for (Healers healer : healers) {
                    if (betweenTrollHealer(troll, healer) < 5 && !healer.isDestroyed()) {
                        healer.trollgetHealer(troll);
                    }
                }
            }

        //crear hordas de elfos
            for(int i=0; i<elfs.length-1;i++){
                for(int j=i+1; j<elfs.length;j++){
                    if(betweenElfs(elfs[i], elfs[j])<=5){
                        int h=elfs[i].getHorde();
                        getElfHordes(elfHordes[h],elfs[i]);
                        getElfHordes(elfHordes[h],elfs[j]);
                        System.out.println("Se creó una horda con el elfo "+elfs[i].getName()+" y el elfo "+elfs[j].getName()+".");
                    }
                }
            }
        //crear hordas de orcos
            for(int i=0; i<orcs.length-1;i++){
                for(int j=i+1;j<orcs.length;j++){
                    if(betweenOrcs(orcs[i],orcs[j])<=5){
                        int h=elfs[i].getHorde();
                        getOrcHordes(orcHordes[h],orcs[i]);
                        getOrcHordes(orcHordes[h],orcs[j]);
                        System.out.println("Se creó una horda con el orco "+orcs[i].getName()+" y el orco "+orcs[j].getName()+".");
                    }
                }
            }

        //poohtazos
            for(int i=0; i<elfs.length;i++){
                for(int j=0; j<orcs.length; j++){
                    if(betweenElfsOrcs(elfs[i], orcs[j])<=5){
                        int hE=elfs[i].getHorde();
                        int hO=orcs[j].getHorde();
                        int numMemE = elfHordes[hE].getNumMembers()-1;
                        int numMemO = orcHordes[hO].getNumMembers()-1;
                        if (numMemE == numMemO) {
                            for (int k = 0; k < numMemE; k++) {
                                oneOnOneFight(elfs[k], orcs[k]);
                            }
                        }
                        else if (numMemE < numMemO) {
                            for (int k = 0; k < numMemE; k++) {
                                oneOnOneFight(elfs[k], orcs[k]);
                            }
                        }
                        else {
                            for (int k = 0; k < numMemO; k++) {
                                oneOnOneFight(elfs[k], orcs[k]);
                            }
                        }
                    }
                }
            }
        //Si se encuentran a un troll
            for (Elfs elf : elfs) {
                for (Trolls troll : trolls) {
                    if (betweenElfsTrolls(elf, troll) <= 5) {
                        elf.setLife(0);
                        System.out.println("Un troll se comió al elfo " + elf.getName() + ".");
                    }
                }
            }
            for (Orcs orc : orcs) {
                for (Trolls troll : trolls) {
                    if (betweenOrcsTrolls(orc, troll) <= 5) {
                        orc.setLife(0);
                        System.out.println("Un troll se comió al orco " + orc.getName() + ".");
                    }
                }
            }
        }

        else{
            System.out.println("Terminó el juego");
        }
    }
    private void play(){
        for(int i=0; i<elfHordes.length; i++){
            elfHordes[i]=new Hordes();
        }
        for(int i=0; i<orcHordes.length; i++){
            orcHordes[i]=new Hordes();
        }
        for(int i=0; i<elfs.length;i++){
            getElfHordes(elfHordes[i],elfs[i]);
        }
        for(int i=0; i<orcs.length;i++){
            getOrcHordes(orcHordes[i],orcs[i]);
        }
        fight();
    }


    private static float distBetween(int x1, int y1, int x2, int y2){
        int x3 = x2-x1;
        int y3 = y2-y1;
        return (float) (Math.sqrt(Math.pow(x3,2)) + Math.sqrt(Math.pow(y3,2)));
    }
    private static float betweenElfs(Elfs a, Elfs b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getCoordx(), b.getCoordy());
    }
    private static float betweenOrcs(Orcs a, Orcs b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getCoordx(), b.getCoordy());
    }
    private static float betweenElfsOrcs(Elfs a, Orcs b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getCoordx(), b.getCoordy());
    }
    private static float betweenElfsTrolls(Elfs a, Trolls b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getCoordx(), b.getCoordy());
    }
    private static float betweenOrcsTrolls(Orcs a, Trolls b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getCoordx(), b.getCoordy());
    }
    private static float betweenElfAmulet(Elfs a, Amulets b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getPx(), b.getPy());
    }
    private static float betweenOrcAmulet(Orcs a, Amulets b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getPx(), b.getPy());
    }
    private static float betweenTrollAmulet(Trolls a, Amulets b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getPx(), b.getPy());
    }
    private static float betweenElfWeapon(Elfs a, Weapons b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getPx(), b.getPy());
    }
    private static float betweenOrcWeapon(Orcs a, Weapons b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getPx(), b.getPy());
    }
    private static float betweenTrollWeapon(Trolls a, Weapons b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getPx(), b.getPy());
    }
    private static float betweenElfHealer(Elfs a, Healers b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getPx(), b.getPy());
    }
    private static float betweenOrcHealer(Orcs a, Healers b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getPx(), b.getPy());
    }
    private static float betweenTrollHealer(Trolls a, Healers b){
        return distBetween(a.getCoordx(), a.getCoordy(), b.getPx(), b.getPy());
    }

    public static void main(String[]args){
       Grid a = new Grid();
       a.play();
}

}
