package com.company;

public class Hordes {
    Creatures[] members = new Creatures[5];
    private int coordX;
    private int coordY;
    private int numMembers;

    public Hordes(){
        coordX=0;
        coordY=0;
        numMembers=0;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }

    public int getCoordX() {
        return coordX;
    }

    public int getCoordY() {
        return coordY;
    }

    public void setMembers(Creatures l) {
        for(int i=0; i<members.length; i++){
            if(null==members[i]){
                members[i]=l;
                numMembers+=1;
            }
        }
    }

    public Creatures getMembers(int i) {
        return members[i];
    }

    public void setNumMembers(int numMembers) {
        this.numMembers = numMembers;
    }

    public int getNumMembers() {
        return numMembers;
    }

    public void killMember(int k){
        numMembers-=k;
    }

    public void centroidX(int x){
        coordX+=x;
        coordX=coordX/numMembers;
    }

    public void centroidY(int y){
        coordY+=y;
        coordY=coordY/numMembers;
    }

}
