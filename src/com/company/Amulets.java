package com.company;

public class Amulets extends Items {
    public Amulets(){

    }

    public void elfgetAmulet (Elfs elf) {
        if(!this.isDestroyed()){
                elf.setMagic(elf.getMagic()+40);
                System.out.println("El elfo "+elf.getName()+" encontró un amuleto");
                System.out.println(elf.getName()+" ahora tiene "+elf.getMagic()+" puntos de magia.");
                this.setDestroyed(true);
        }
    }

    public void orcgetAmulet(Orcs orc){
        if(!this.isDestroyed()){
            System.out.println("El orco "+orc.getName()+" encontró un amuleto y lo destruyó.");
            this.setDestroyed(true);
        }
    }

    public void trollgetAmulet(Trolls troll){
        if(!this.isDestroyed()){
            System.out.println("Un troll encontró un amuleto y lo destruyó.");
            this.setDestroyed(true);
        }
    }



}
