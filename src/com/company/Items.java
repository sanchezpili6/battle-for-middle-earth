package com.company;

public class Items {
    private int px;
    private int py;
    private boolean destroyed;

    public Items(){
        px = Orcs.random(0,100);
        py = Orcs.random(0,100);
    }

    public int getPx() {
        return px;
    }

    public void setPx(int px) {
        this.px = px;
    }

    public int getPy() {
        return py;
    }

    public void setPy(int py) {
        this.py = py;
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public void setDestroyed(boolean destroyed) {
        this.destroyed = destroyed;
    }

}
