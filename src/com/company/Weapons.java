package com.company;

public class Weapons extends Items {
    public Weapons() {
    }

    public void elfgetWeapon(Elfs elf) {
        if (!this.isDestroyed()) {
            elf.setStrength(elf.getStrength() + 10);
            System.out.println("El elfo " +elf.getName()+ " encontró un arma.");
            System.out.println(elf.getName()+" ahora tiene una fuerza de " + elf.getStrength() + ".");
            this.setDestroyed(true);
        }
    }

    public void orcgetWeapon(Orcs orc) {
        if (!this.isDestroyed()) {
            orc.setStrength(orc.getStrength() + 30);
            System.out.println("El orco "+orc.getName()+" encontró un arma.");
            System.out.println(orc.getName()+" ahora tiene una fuerza de " + orc.getStrength() + ".");
            this.setDestroyed(true);
        }
    }

    public void trollgetWeapon(Trolls troll) {
        if (!this.isDestroyed()) {
            System.out.println("Un troll encontró un arma y la destruyó.");
            this.setDestroyed(true);
        }
    }

}
