package com.company;
public class Creatures {
    private int strength;
    private int magic;
    private float damageDone;
    private float life;
    private int speedX;
    private int speedY;
    private int coordx;
    private int coordy;
    int horde;
    private String name;

//    orc=0;
//    elf=1;
//    troll=2;

    public Creatures() {
        coordx = Orcs.random(0,100);
        coordy = Orcs.random(0,100);
        speedX = Orcs.random(1,3);
        speedY = Orcs.random(1,3);
    }

   void moveX(){
       if(coordx+speedX>100){
           coordx=coordx-speedX;
       }
       else{
           coordx=coordx+speedX;
       }
   }

   void moveY(){
        if(coordy+speedY>100){
            coordy=coordy-speedY;
        }
        else {
            coordy=coordy+speedY;
        }
   }

    int getStrength() {
        return this.strength;
    }

    void setStrength(int strength) {
        this.strength = strength;
    }

    public void setDamageDone(float damageDone) {
        this.damageDone = damageDone;
    }

    float getDamageDone() {
        return damageDone;
    }

    int getMagic() {
        return magic;
    }

    void setMagic(int magic) {
        this.magic = magic;
    }

    float getLife() {
        return life;
    }

    void setLife(float life) {
        this.life = life;
    }

    void setName(String name) {
        this.name = name;
    }

    String getName(){
        return name;
    }

    int getCoordx() {
        return coordx;
    }

    void setCoordx(int coordx) {
        this.coordx = coordx;
    }

    int getCoordy() {
        return coordy;
    }

    void setCoordy(int coordy) {
        this.coordy = coordy;
    }

    boolean isAlive() {
        return !(this.life <= 0);
    }


}
